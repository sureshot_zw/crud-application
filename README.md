# CRUD-Application

This is a Basic CRUD application for managing users with roles. I tried to do some parts well and to also not overengineer.
I chose a basic implimentation of roles without including use of packages though the later is not complex either especially
when roles and permissions are required.
I am not a Front End guru (back-end is where i am strongest) though I know how to get around VueJS as well as CSS.
Everything is run in a docker environment.
A working assumption I have is that you have docker setup on your machine and can use the `docker-compose` command.
You may need to google or read up on how to get this working if you do not, some helpful options:
* [Install Docker Compose On Ubuntu 18](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04)
* [Install Docker Compose On Ubuntu 20](https://phoenixnap.com/kb/install-docker-compose-on-ubuntu-20-04)


## STACK
* HTML
* CSS (Boostrap)
* VUEJS
* MYSQL
* DOCKER

## HOW TO GET IT RUNNING

I tried to make your life simple, all things being equal you should be able to run `sh init.sh` within the project folder.
What will happen when you run this command:

* Sets up the docker contains.
* Sets permissions inside docker.
* Adds the environmental file for laravel.
* Installs composer dependecies.
* Generates app key for laravel.
* Caches config.
* Migrates databases.
* Runs DB seeders.
* Creates an admin user through a CLI command.
* Runs unit tests.
* Installs and compiles FE dependencies. 

**This application was developed in a linux environment.**

If all goes well you should be able to visit [Local Host](http://localhost/login) and login with the credentials (put them here to get you going quick)

* **U** : `testperson@example.com`
* **P** : `BIGback123@`

You can use the same CLI command to create your own admin users and if you ever use this application or borrow from it for something else that 
may end up in a public space, you would want to not have the default user created automatically with credentials resident in version control
as that is a security risk. 

## Some Thoughts
* I would consider using [Laravel Permission](https://github.com/spatie/laravel-permission) for production applications needing to impliment roles and permissions.
* Coupled with unit testing, I believe intergrating DUSK for FE testing would be really neat and useful [Laravel Dusk](https://laravel.com/docs/7.x/dusk).
* For more consistent system generated passwords that can abide by required password schemas you can use [Hackzilla Password Generator](https://packagist.org/packages/hackzilla/password-generator).
