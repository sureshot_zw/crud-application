#!/bin/bash

# back end dependencies
docker-compose up -d
docker-compose exec crud-application bash -c "chown -R www-data.www-data /app"
docker-compose exec crud-application bash -c "chmod -R guo+w /app/storage"
docker-compose exec crud-application bash -c "cp /app/.env.example /app/.env"
docker-compose exec crud-application bash -c "cd /app && composer install"
docker-compose exec crud-application bash -c "cd /app && php artisan key:generate"
docker-compose exec crud-application bash -c "cd /app && php artisan cache:clear"
docker-compose exec crud-application bash -c "cd /app && php artisan config:clear"
docker-compose exec crud-application bash -c "cd /app && php artisan view:clear"
docker-compose exec crud-application bash -c "cd /app && php artisan migrate"
docker-compose exec crud-application bash -c "cd /app && composer dumpautoload"
docker-compose exec crud-application bash -c "cd /app && php artisan db:seed"

#the next action is a sample, in a production system adding this to version control posses a security risk
docker-compose exec crud-application bash -c "cd /app && php artisan administration:add-admin-user 'Test Person' testperson@example.com BIGback123@"

# unit tests
docker-compose exec crud-application bash -c "cd /app && vendor/bin/phpunit"

#fe dependencies
docker-compose exec crud-application-fe sh -c "cd /app && npm install"
docker-compose exec crud-application-fe sh -c "cd /app && npm run dev"	
