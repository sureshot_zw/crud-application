<?php

namespace Tests\Unit;

use App\Role;
use Tests\TestCase;
use App\PasswordValidation;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class PasswordValidationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Setup override
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed');
    }
    
    /**
     * Test admin role password validatiom
     *
     * @return void
     */
    public function testAdminRolePassword()
    {
        $adminRole = Role::where('name', 'admin')->first();

        $this->validateSimpleAdminPassword($adminRole, 'xyzab');
        $this->validateComplexAdminPassword($adminRole, 'ErtP$PAz7&');
        $this->validateSemiComplexAdminPassword($adminRole, 'password@$');
    }

    /**
     * Validate simple password
     * 
     * @param  Role   $role     
     * @param  string $password 
     * @return void
     */
    protected function validateSimpleAdminPassword(Role $role, string $password)
    {
        $passwordValidationService = new PasswordValidation($role, $password);

        $this->assertFalse($passwordValidationService->validateLength());
        $this->assertFalse($passwordValidationService->hasUCaseCharacter());
        $this->assertFalse($passwordValidationService->hasDigits());
        $this->assertFalse($passwordValidationService->hasSpecialsCharacter());

    }

    /**
     * Validate complex password
     * 
     * @param  Role   $role     
     * @param  string $password 
     * @return void
     */
    protected function validateComplexAdminPassword(Role $role, string $password)
    {
        $passwordValidationService = new PasswordValidation($role, $password);
        $this->assertTrue($passwordValidationService->validateLength());
        $this->assertTrue($passwordValidationService->hasUCaseCharacter());
        $this->assertTrue($passwordValidationService->hasDigits());
        $this->assertTrue($passwordValidationService->hasSpecialsCharacter());
    }

    /**
     * Validate semi complex password
     * 
     * @param  Role   $role     
     * @param  string $password 
     * @return void
     */
    protected function validateSemiComplexAdminPassword(Role $role, string $password)
    {
        $passwordValidationService = new PasswordValidation($role, $password);
        $this->assertTrue($passwordValidationService->validateLength());
        $this->assertFalse($passwordValidationService->hasUCaseCharacter());
        $this->assertFalse($passwordValidationService->hasDigits());
        $this->assertTrue($passwordValidationService->hasSpecialsCharacter());
    }

    /**
     * Test user role password validation
     *
     * @return void
     */
    public function testUserRolePassword()
    {
        $userRole = Role::where('name', 'user')->first();

        $this->validateSimpleUserPassword($userRole, 'xyzx');
        $this->validateComplexUserPassword($userRole, 'y@Du$6U7H$');
        $this->validateSemiComplexUserPassword($userRole, 'dH#sxYR');
    }

    /**
     * Validate simple user password
     * 
     * @param  Role   $role     
     * @param  string $password 
     * @return void           
     */
    protected function validateSimpleUserPassword(Role $role, string $password)
    {
        $passwordValidationService = new PasswordValidation($role, $password);
        $this->assertTrue($passwordValidationService->validateLength());
        $this->assertFalse($passwordValidationService->hasUCaseCharacter());
        $this->assertFalse($passwordValidationService->hasDigits());
        $this->assertFalse($passwordValidationService->hasSpecialsCharacter());
    }

    /**
     * Validate complex user password
     * 
     * @param  Role   $role     
     * @param  string $password 
     * @return void           
     */
    protected function validateComplexUserPassword(Role $role, string $password)
    {
        $passwordValidationService = new PasswordValidation($role, $password);
        $this->assertTrue($passwordValidationService->validateLength());
        $this->assertTrue($passwordValidationService->hasUCaseCharacter());
        $this->assertTrue($passwordValidationService->hasDigits());
        $this->assertTrue($passwordValidationService->hasSpecialsCharacter());
    }

    /**
     * Validate semi complex user password
     * 
     * @param  Role   $role     
     * @param  string $password 
     * @return void           
     */
    protected function validateSemiComplexUserPassword(Role $role, string $password)
    {
        $passwordValidationService = new PasswordValidation($role, $password);
        $this->assertTrue($passwordValidationService->validateLength());
        $this->assertTrue($passwordValidationService->hasUCaseCharacter());
        $this->assertFalse($passwordValidationService->hasDigits());
        $this->assertTrue($passwordValidationService->hasSpecialsCharacter());
    }
}
