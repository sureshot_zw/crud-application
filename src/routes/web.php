<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('users')->group(function () {
    Route::get('/view-add-user', [UserController::class, 'viewAddUser'])->name('viewAdduser');
    Route::get('/view-add-user/{user}', [UserController::class, 'viewAddUser'])->name('viewAdduserWithId');
    Route::post('/add', [UserController::class, 'add'])->name('addUser');
    Route::post('/search', [UserController::class, 'searchUsers'])->name('searchUsers');
    Route::get('/view-users', [UserController::class, 'view'])->name('viewUsers');
    Route::get('/get', [UserController::class, 'get'])->name('getUser');
    Route::get('/get/{user}', [UserController::class, 'getById'])->name('getUserById');
    Route::put('/update/{user}', [UserController::class, 'update'])->name('updateUser');
    Route::delete('/delete/{user}', [UserController::class, 'delete'])->name('deleteUser');
});


Route::prefix('roles')->group(function () {
    Route::get('/view-add-role', [RolesController::class, 'viewAddRole'])->name('viewAddRole');
    Route::get('/view-add-role/{role}', [RolesController::class, 'viewAddRole'])->name('viewAddRoleWithId');
    Route::post('/add', [RolesController::class, 'add'])->name('addRole');
    Route::post('/search', [RolesController::class, 'searchRoles'])->name('searchRoles');
    Route::get('/view-roles', [RolesController::class, 'view'])->name('viewRoles');
    Route::get('/get', [RolesController::class, 'get'])->name('getRoles');
    Route::get('/get-all', [RolesController::class, 'getAllRoles'])->name('getAllRoles');
    Route::get('/get/{role}', [RolesController::class, 'getById'])->name('getRolesById');
    Route::put('/update/{role}', [RolesController::class, 'update'])->name('updateRole');
    Route::delete('/delete/{role}', [RolesController::class, 'delete'])->name('deleteRole');
});

