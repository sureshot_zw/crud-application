<?php 

namespace App\Traits;

use App\Rules\PasswordLengthValidator;
use App\Rules\PasswordDigitsValidator;
use App\Rules\PasswordUCaseValidator;
use App\Rules\PasswordSpecialCharactersValidator;

trait PasswordValidationRuleTrait
{
	/**
     * Password validation rules
     *
     * @param Integer $role
     * @param bool $makeNullable
     * 
     * @return array
     */
    protected function passwordValidationRules(int $role, bool $makeNullable = false)
    {
        return [
            !$makeNullable ? 'required' : 'sometimes', 
            'string', 
            'confirmed',
            new PasswordLengthValidator($role),
            new PasswordDigitsValidator($role),
            new PasswordUCaseValidator($role),
            new PasswordSpecialCharactersValidator($role),
        ];
    }
}