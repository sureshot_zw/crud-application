<?php 

namespace App\Traits;

use Illuminate\Pagination\Paginator;

trait PaginatorMethodsTrait
{
	/**
	 * Set current page for pagination results
	 *
	 * @param int $currentPage
	 *
	 * @return void
	 */
	protected function setCurrentPageForPagination(int $currentPage)
	{
		Paginator::currentPageResolver(function() use ($currentPage) {
			return $currentPage;
		});
	}
}