<?php

namespace App;

class PasswordValidation
{

	/**
	 * Role
	 * 
	 * @var Role
	 */
	protected $role;

	/**
	 * Password 
	 * 
	 * @var String
	 */
	protected $password;

	/**
	 * Default constructor
	 * 
	 * @param Role $role
	 * @param  string $password 
	 */
	public function __construct(Role $role, string $password)
	{
		$this->role = $role;
		$this->password = $password;
	}


	/**
	 * Validate password length
	 *
	 * @return boolean
	 */
	public function validateLength() : bool
	{
		return strlen($this->password) >= $this->role->password_min_chars;
	}

	/**
	 * Password has at least one upper case letter
	 * 
	 * @return boolean 
	 */
	public function hasUCaseCharacter() : bool
	{
		return preg_match_all('/[A-Z]/m', $this->password);
	}

	/**
	 * Has at least one digit
	 * 
	 * @return boolean
	 */
	public function hasDigits() : bool
	{
		return preg_match_all('/[0-9]/m', $this->password);
	}

	/**
	 * Has at least one special character
	 * 
	 * @return boolean 
	 */
	public function hasSpecialsCharacter() : bool
	{
		return preg_match_all('/[@$!%*#?&]/m', $this->password);
	}
}


