<?php

namespace App\Rules;

use App\PasswordValidation as PasswordValidationService;
use App\Role;
use Illuminate\Contracts\Validation\Rule;

class PasswordLengthValidator implements Rule
{
    /**
     * Data
     * 
     * @var array
     */
    protected $role;

    /**
     * Create a new rule instance.
     *
     * @param int $role
     *
     * @return void
     */
    public function __construct(int $role)
    {
        $this->role = Role::find($role);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validationService = new PasswordValidationService($this->role, $value);

        return !$validationService->validateLength() ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Password length should be at least ' . $this->role->password_min_chars . ' characters long';
    }
}
