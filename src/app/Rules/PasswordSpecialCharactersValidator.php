<?php

namespace App\Rules;

use App\PasswordValidation as PasswordValidationService;
use App\Role;
use Illuminate\Contracts\Validation\Rule;

class PasswordSpecialCharactersValidator implements Rule
{
    /**
     * Data
     * 
     * @var array
     */
    protected $role;

    /**
     * Create a new rule instance.
     *
     * @param int $role
     *
     * @return void
     */
    public function __construct(int $role)
    {
        $this->role = Role::find($role);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validationService = new PasswordValidationService($this->role, $value);

        return $this->role->include_special_chars && !$validationService->hasSpecialsCharacter() ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Password should have at least one special character [@$!%*#?&]';
    }
}
