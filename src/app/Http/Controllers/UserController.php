<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Http\Requests\AddNewUser;
use App\Http\Requests\UpdateUser;
use App\Http\Requests\SearchUsers;
use App\Traits\PaginatorMethodsTrait;
use Log;
use Hash;
use Redirect;

class UserController extends Controller
{
    use PaginatorMethodsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Search users
     * 
     * @param  SearchUsers $request 
     * 
     * @return Response          
     */
    public function searchUsers(SearchUsers $request)
    {
        $params = (object) $request->all();

        $this->setCurrentPageForPagination($params->page);

        return User::with('role')
            ->where('name', 'LIKE', '%' . $params->search . '%')
            ->orWhere('email', 'LIKE', '%' . $params->search . '%')
            ->orWhere('username', 'LIKE', '%' . $params->search . '%')
            ->orWhere('id', $params->search)
            ->orderBy('id', 'DESC')
            ->paginate($params->limit);
    }

    /**
     * View add user
     * 
     * @param  Request $request 
     * @param  User    $user   
     *  
     * @return View          
     */
    public function viewAddUser(Request $request, User $user)
    {
        $isAdmin = request()->user()->isAdmin();
        if(!$isAdmin) {
            return Redirect::back()
            ->withErrors([
                'message' => 'You do not have sufficient permission to view the requested page'
            ]);
        }
        
        return view('users.addUser', [
            'user' => $user ? $user->id : null,
            'is_admin' => $isAdmin,
        ]);
    }

    /**
     * Add user request
     * 
     * @param AddNewUser $request
     *
     * @return JsonReponse
     */
    public function add(AddNewUser $request)
    {
        if(request()->user()->isAdmin() === false) {
            return response()->json([
                'success' => false,
                'message' => 'You do not have sufficient permission to carry out this action'
            ]); 
        }

        try {
            $data = $request->all();
            $data['password'] = Hash::make($data['password']);

            User::create($data);

            return response()->json([
                'success' => true,
                'message' => '',
                'redirect_url' => route('viewUsers')
            ]);
        }catch(Exception $e)
        {
            Log::error($e->getMessage(), $request->all());

            return response()->json([
                'success' => false,
                'message' => 'An error occured while trying to add the user',
            ]);
        }
    }

    /**
     * View page for list of users
     * 
     * @param Request
     *
     * @return JsonReponse
     */
    public function view(Request $request)
    {
        return view('users.usersList', [
            'is_admin' => request()->user()->isAdmin(),
        ]);
    }

    /**
     * Get users request
     * 
     * @param Request
     *
     * @return JsonReponse
     */
    public function get(Request $request)
    {
        $params = (object) $request->all();

        $this->setCurrentPageForPagination($params->page);

        return User::with('role')->orderBy('id', 'DESC')->paginate($params->limit);
    }

    /**
     * Get Role request
     * 
     * @param Request $request
     * @param User $user
     *
     * @return JsonReponse
     */
    public function getById(Request $request, $user)
    {
        return User::with('role')->where('id', $user)->first();
    }

    /**
     * Update user request
     * 
     * @param UpdateUser $request
     * @param User $user
     *
     * @return JsonReponse
     */
    public function update(UpdateUser $request, User $user)
    {
        if(request()->user()->isAdmin() === false) {
            return response()->json([
                'success' => false,
                'message' => 'You do not have sufficient permission to carry out this action'
            ]); 
        }

        try {
            $data = (object)$request->all();
            $user->name = $data->name;
            $user->email = $data->email;
            $user->username = $data->username;

            if(isset($data->password)) {
                $user->password = Hash::make($data->password);
            }

            $user->role = $data->role;

            $user->save();  

            return response()->json([
                'success' => true,
                'message' => ''
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage(), $request->all());

            return response()->json([
                'success' => false,
                'message' => 'An error occured while trying to update the user'
            ]);
        }

    }

    /**
     * Delete user request
     * 
     * @param Request $request
     * @param User $user
     *
     * @return JsonReponse
     */
    public function delete(Request $request, User $user)
    {
        if(request()->user()->isAdmin() === false) {
            return response()->json([
                'success' => false,
                'message' => 'You do not have sufficient permission to carry out this action'
            ]); 
        }

        if(request()->user()->id === $user->id) {
            return response()->json([
                'success' => false,
                'message' => 'You cannot delete yourself'
            ]); 
        }

        try
        {
            $user->delete();

            return response()->json([
                'success' => true,
                'message' => 'User successfully deleted',
                'redirect_url' => route('viewUsers')
            ]);

        }catch(Exception $ex)
        {
            Log::emergency($ex->getMessage(), [
                'user' => $user->id
            ]);

            return response()->json([
                'success' => false,
                'message' => 'An error occured while attempting to delete the user',
            ]);
        }
    }
}
