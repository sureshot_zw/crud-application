<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Traits\PaginatorMethodsTrait;
use App\Http\Requests\UpdateRole;
use App\Http\Requests\AddNewRole;
use App\Http\Requests\SearchRoles;
use Log;
use Redirect;

class RolesController extends Controller
{
    use PaginatorMethodsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Search Roles
     * 
     * @param  SearchRoles $request 
     * 
     * @return Response          
     */
    public function searchRoles(SearchRoles $request)
    {
        $params = (object) $request->all();

        $this->setCurrentPageForPagination($params->page);

        return Role::where('name', 'LIKE', '%' . $params->search . '%')
            ->orWhere('id', $params->search)
            ->orderBy('id', 'DESC')
            ->paginate($params->limit);
    }

    /**
     * View Add role
     * 
     * @param  Request $request 
     * @param  Role    $role  
     *   
     * @return View          
     */
    public function viewAddRole(Request $request, Role $role)
    {
        $isAdmin = request()->user()->isAdmin();
        if(!$isAdmin) {
            return Redirect::back()
            ->withErrors([
                'message' => 'You do not have sufficient permission to view the requested page'
            ]);
        }
        
        return view('roles.addRole', [
            'role_id' => $role ? $role->id : null,
            'is_admin' => $isAdmin,
        ]);
    }

    /**
     * Add Role request
     * 
     * @param AddNewRole $request
     *
     * @return JsonReponse
     */
    public function add(AddNewRole $request)
    {
        if(request()->user()->isAdmin() === false) {
            return response()->json([
                'success' => false,
                'message' => 'You do not have sufficient permission to carry out this action'
            ]); 
        }

        try {
            $data = $request->all();

            Role::create($data);

            return response()->json([
                'success' => true,
                'message' => '',
                'redirect_url' => route('viewRoles')
            ]);
        }catch(Exception $e)
        {
            Log::error($e->getMessage(), $request->all());

            return response()->json([
                'success' => false,
                'message' => 'An error occured while trying to add the role',
            ]);
        }
    }

    /**
     * View all roles
     * 
     * @param Request
     *
     * @return JsonReponse
     */
    public function view(Request $request)
    {
        return view('roles.roleList', [
            'is_admin' => request()->user()->isAdmin(),
        ]);
    }

    /**
     * Get Role request
     * 
     * @param Request
     *
     * @return JsonReponse
     */
    public function get(Request $request)
    {
        $params = (object) $request->all();

        $this->setCurrentPageForPagination($params->page);

        return Role::orderBy('id', 'DESC')
            ->paginate($params->limit);
    }


    /**
     * Get all roles
     * 
     * @param Request
     *
     * @return JsonReponse
     */
    public function getAllRoles(Request $request)
    {

        return Role::all();
    }

    /**
     * Get Role request
     * 
     * @param Request $request
     * @param $role
     *
     * @return JsonReponse
     */
    public function getById(Request $request, $role)
    {
        return Role::where('id', $role)->first();
    }

    /**
     * Update Role request
     * 
     * @param UpdateRole $request
     * @param Request $role
     *
     * @return JsonReponse
     */
    public function update(UpdateRole $request, Role $role)
    {
        if(request()->user()->isAdmin() === false) {
            return response()->json([
                'success' => false,
                'message' => 'You do not have sufficient permission to carry out this action'
            ]); 
        }

        try {
            $data = (object)$request->all();
            $role->description = $data->description; 
            $role->password_min_chars = $data->password_min_chars; 
            $role->include_caps = $data->include_caps; 
            $role->include_digits = $data->include_digits; 
            $role->include_special_chars = $data->include_special_chars;
            $role->save();  

            return response()->json([
                'success' => true,
                'message' => ''
            ]);
        } catch (Exception $e) {
            Log::error($e->getMessage(), $request->all());

            return response()->json([
                'success' => false,
                'message' => 'An error occured while trying to update the role'
            ]);
        }
    }

    /**
     * Delete Role request
     * 
     * @param Request $request
     * @param Request $role
     *
     * @return JsonReponse
     */
    public function delete(Request $request, Role $role)
    {
        if(request()->user()->isAdmin() === false) {
            return response()->json([
                'success' => false,
                'message' => 'You do not have sufficient permission to carry out this action'
            ]); 
        }

        if($role->isAdminRole() || $role->isUserRole()) {
            return response()->json([
                'success' => false,
                'message' => $role->name .' role cannot be deleted'
            ]);
        }

        try
        {
            if(strtolower($role->name) === 'admin') {
                return response()->json([
                    'success' => false,
                    'message' => 'You cannot delete the admin role',
                ]);
            }

            $userRole = Role::where('name', 'user')->first();

            // ensure users that had the role are defaulted to users
            User::where('role', $role->id)->update(['role' => $userRole->id]);

            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role successfully deleted',
                'redirect_url' => route('viewUsers')
            ]);

        }catch(Exception $ex)
        {
            Log::emergency($ex->getMessage(), [
                'role' => $role->id
            ]);

            return response()->json([
                'success' => false,
                'message' => 'An error occured while attempting to delete the role',
            ]);
        }
    }
}
