<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;
use App\Traits\PasswordValidationRuleTrait;


class UpdateUser extends FormRequest
{
    use SanitizesInput;
    use PasswordValidationRuleTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     *  Validation rules to be applied to the input.
     *
     *  @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'username' => ['required', 'string'],
            'password' => $this->passwordValidationRules($this->role, true),
            'password_confirmation' => ['nullable', 'same:password'],
            'role' => ['required', 'exists:roles,id'],

        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     *  @return array
     */
    public function filters()
    {
        return [
            'name' => 'trim|escape|capitalize',
            'email' =>'trim|escape',
            'username' => 'trim|escape|lowercase',
            'password' => 'trim|escape',
            'confirm_password' => 'trim|escape',
        ];
    }
}
