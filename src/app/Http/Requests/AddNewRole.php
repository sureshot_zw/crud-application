<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;

class AddNewRole extends FormRequest
{
    use SanitizesInput;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() ;
    }

    /**
     *  Validation rules to be applied to the input.
     *
     *  @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'unique:roles'], 
            'description' => ['nullable', 'string'], 
            'password_min_chars' => ['required', 'numeric'], 
            'include_caps' => ['nullable'], 
            'include_digits' => ['nullable'], 
            'include_special_char' => ['nullable']
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     *  @return array
     */
    public function filters()
    {
        return [
            'name' => 'trim|escape', 
            'description' => 'trim|escape', 
            'password_min_chars' => 'trim', 
            'include_caps' => 'cast:boolean', 
            'include_digits' => 'cast:boolean', 
            'include_special_chars' => 'cast:boolean'
        ];
    }
}
