<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Role;
use App\User;
use Illuminate\Support\Str;
use App\Traits\PasswordValidationRuleTrait;
use Validator;
use Hash;

class AddAdminUser extends Command
{
    use PasswordValidationRuleTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'administration:add-admin-user {full_name} {email_address} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to add admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $role = Role::where('name', 'admin')->first();

        if(empty($role)) {
            $this->error('Role for administrators is missing, please ensure you have run database seeders first');

            return;
        }

        $email = $this->argument('email_address');
        $password = $this->argument('password');
        $data = [
            'name' => $this->argument('full_name'),
            'email' => $email,
            'password' => $password,
            // since we are using a validator more suited to front end 
            // we have ensure continuity of execution as password confirmation is required
            'password_confirmation' => $password, 
            'role' => $role->id,
            'username' => Str::before($email, '@'),
        ];

        $validator = Validator::make($data, [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users'],
            'username' => ['required', 'string', 'unique:users'],
            'password' => $this->passwordValidationRules($role->id),
            'password_confirmation' => ['required', 'same:password'],
            'role' => ['required', 'exists:roles,id'],

        ]);

        if ($validator->fails()) {
            collect($validator->messages()->get('*'))->values()
                ->flatten(1)
                ->each(function($error) {
                    $this->error($error);
            });

            return;
        }


        try {
            $data['password'] = Hash::make($data['password']);
            User::create($data);
        }catch(Exception $ex)
        {
            $this->error($ex->getMessage());

            $this->comment('Admin user creation failed');
        }

        $this->comment('Admin user successfully created');

    }
}
