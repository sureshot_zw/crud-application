<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'description', 
        'password_min_chars', 
        'include_caps', 
        'include_digits', 
        'include_special_chars'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'include_caps' => 'boolean',
        'include_digits' => 'boolean',
        'include_special_chars' => 'boolean',
    ];

    /**
     * Hidden fields
     * 
     * @var array
     */
    protected $hidden = [
        'password_min_chars', 'include_caps', 'include_digits', 'include_special_chars'
    ];

    /**
     * Timestamps
     * 
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Get the user's role name.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * To array override
     * 
     * @return array
     */
    public function toArray()
    {
        // Only hide email if `guest` or not an `admin`
        if (auth()->check() && auth()->user()->isAdmin()) {
            $this->setAttributeVisibility();
        }

        return parent::toArray();
    }

    /**
     * Set attribute availabiliy
     *
     * @return void
     */
    public function setAttributeVisibility()
    {
        $this->makeVisible(array_merge($this->fillable, $this->appends, ['enter_relationship_or_other_needed_data']));
    }

    /**
     * Is admin role
     * 
     * @return boolean 
     */
    public function isAdminRole()
    {
        return strtolower($this->name)=== 'admin';
    }

    /**
     * Is user role
     * 
     * @return boolean 
     */
    public function isUserRole()
    {
        return strtolower($this->name) === 'user';
    }
}
