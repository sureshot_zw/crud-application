<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Role::whereIn('name', ['user', 'admin'])->get()->isNotEmpty()) {
            throw new Exception('Roles already exist, seeder has been run before');
        }


        Role::create([
            'name' => 'user', 
            'description' => 'User role', 
            'password_min_chars' => 4, 
            'include_caps' => true, 
            'include_digits' => true, 
            'include_special_chars' => false,
        ]);

        Role::create([
            'name' => 'admin', 
            'description' => 'Administrator role', 
            'password_min_chars' => 8, 
            'include_caps' => true, 
            'include_digits' => true, 
            'include_special_chars' => true,
        ]);

    }
}
