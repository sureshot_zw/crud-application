@extends('layouts.app')

@section('content')
	<div id="users" class="container">
		@if($errors->any())
			<div class="bg-danger p-2">
				<h4>{{$errors->first()}}</h4>
			</div>
		@endif

		<view-users is_admin="{{$is_admin}}"></view-users>
	</div>
@endsection