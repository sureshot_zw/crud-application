@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Quick Actions') }}</div>

                <div class="card-body">
                    @if($errors->any())
                        <div class="bg-danger p-2">
                            <h4>{{$errors->first()}}</h4>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(request()->user()->isAdmin())
                        <a class="btn btn-primary mt-1" href="{{route('viewAddRole')}}">Add Role</a>
                        <a class="btn btn-primary mt-1" href="{{route('viewAdduser')}}">Add User</a>
                    @endif
                    <a class="btn btn-primary mt-1" href="{{route('viewRoles')}}">View Roles</a>
                    <a class="btn btn-primary mt-1" href="{{route('viewUsers')}}">View Users</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
