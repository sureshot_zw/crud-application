@extends('layouts.app')

@section('content')
	<div id="users" class="container">
		@if($errors->any())
			<div class="bg-danger p-2">
				<h4>{{$errors->first()}}</h4>
			</div>
		@endif
		<add-role role_id="{{$role_id}}" is_admin="{{$is_admin}}"></add-role>
	</div>
@endsection